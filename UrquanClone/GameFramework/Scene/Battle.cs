﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UrquanClone.GameFramework.GameObject;
using UrquanClone.GameFramework.GameObject.Weapon;
using UrquanClone.GameFramework.Input;
using UrquanClone.GameFramework.Util;

namespace UrquanClone.GameFramework.Scene
{
    public class Battle : IScene
    {
        private readonly Background _background;

        private int _player1Lives = 1;
        private int _player2Lives = 1;

        private List<Bullet> P1Bullets = new List<Bullet>();
        private List<Bullet> P2Bullets = new List<Bullet>();

        private Ship _player1;
        private Ship _player2;


        public Battle()
        {
            _background = new Background();

            GameManager.PlayerShips[PlayerIndex.One].PlayerInput = GameManager.Players[PlayerIndex.One];
            GameManager.PlayerShips[PlayerIndex.Two].PlayerInput = GameManager.Players[PlayerIndex.Two];

            _player1 = GameManager.PlayerShips[PlayerIndex.One];
            _player2 = GameManager.PlayerShips[PlayerIndex.Two];

            _player1Lives = _player1.Life;
            _player2Lives = _player2.Life;

            _player1.Position = new Vector2(100,200);
            _player2.Position = new Vector2(700,400);
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _background.Draw(gameTime, spriteBatch);

            foreach (Bullet bullet in P1Bullets)
            {
                bullet.Draw(gameTime, spriteBatch);
            }

            foreach (Bullet bullet in P2Bullets)
            {
                bullet.Draw(gameTime, spriteBatch);
            }

            _player1.Draw(gameTime, spriteBatch);
            _player2.Draw(gameTime, spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            _player1.PlayerInput.Update();
            _player2.PlayerInput.Update();
            
            _player1.Update(gameTime);
            _player2.Update(gameTime);


            if (_player1.PlayerInput.Shot1() && P1Bullets.Count <= 5)
            {
                var bullet = new Bullet
                {
                    Angle = _player1.Angle,
                    Position = _player1.Position
                };
                P1Bullets.Add(bullet);
            }


            if (_player1.PlayerInput.Shot2() && P1Bullets.Count <= 5)
            {
                var missile = new HomingMissile(_player1, _player2)
                {
                    Angle = _player1.Angle,
                    Position = _player1.Position
                };

                P1Bullets.Add(missile);
            }

            if (_player2.PlayerInput.Shot1() && P2Bullets.Count <= 5)
            {
                Debug.WriteLine("P2 Shot");
                var bullet = new Bullet
                {
                    Angle = _player2.Angle,
                    Position = _player2.Position
                };
                P2Bullets.Add(bullet);
            }

            if (_player2.PlayerInput.Shot2() && P2Bullets.Count <= 5)
            {
                var missile = new HomingMissile(_player2, _player1)
                {
                    Angle = _player2.Angle,
                    Position = _player2.Position
                };

                P2Bullets.Add(missile);
            }

            foreach (Bullet bullet in P1Bullets.ToList())
            {
                bullet.Update(gameTime);
                
                if (bullet.Position.X > GameManager.ScreenSize.X || bullet.Position.Y > GameManager.ScreenSize.Y ||
                    bullet.Position.X < 0 || bullet.Position.Y < 0)
                {
                    P1Bullets.Remove(bullet);
                }

                if (MathUtil.Collide(_player2.BoundingBox, bullet.BoundingBox))
                {
                    _player2Lives--;
                    P1Bullets.Remove(bullet);
                }
            }

            foreach (Bullet bullet in P2Bullets.ToList())
            {
                bullet.Update(gameTime);

                if (bullet.Position.X > GameManager.ScreenSize.X || bullet.Position.Y > GameManager.ScreenSize.Y ||
                    bullet.Position.X < 0 || bullet.Position.Y < 0)
                {
                    P2Bullets.Remove(bullet);
                }

                if (MathUtil.Collide(_player1.BoundingBox, bullet.BoundingBox))
                {
                    _player1Lives--;
                    P2Bullets.Remove(bullet);
                }
            }

            if (_player2Lives <= 0) {
                GameManager.Winner = 1;
                Game1.Scene = GameManager.ChangeState(GameState.GameOver);
            }

            if (_player1Lives <= 0)
            {
                GameManager.Winner = 2;
                Game1.Scene = GameManager.ChangeState(GameState.GameOver);
            }
        }

        public void LoadContent()
        {
        }

        public void Dispose()
        {
        }
    }
}