﻿using System;
using Microsoft.Xna.Framework;

namespace UrquanClone.GameFramework.Util
{
    public class MathUtil
    {
        public static float ToRadians(float degree)
        {
            return (float)(Math.PI / 180) * degree;
        }

        public static Vector2 NewBySizeAngle(float size, float radians)
        {
            return new Vector2(
                (float) Math.Cos(radians) * size,
                (float) Math.Sin(radians) * size
            );
        }

        public static bool Collide(Rectangle main, Rectangle other)
        {
            return main.Intersects(other);
        }
    }
}