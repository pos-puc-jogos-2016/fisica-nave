﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UrquanClone.GameFramework.Scene
{
    public class About : IScene
    {
        private readonly Background _background;
        private readonly SpriteFont _font;
        private SpriteFont _fontTitle;

        public About()
        {
            _background = new Background();
            _font = Game1.ContentManager.Load<SpriteFont>("Font/KenVectorThinSquare");
            _fontTitle = Game1.ContentManager.Load<SpriteFont>("Font/KenVectorFutureTitle");
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _background.Draw(gameTime, spriteBatch);

            spriteBatch.Begin();
            GraphicsDevice graphics = spriteBatch.GraphicsDevice;


            #region AboutMe

            string aboutMeText = "Desenvolvido por Andre Gavazzoni";
            Vector2 aboutMeBounds = _font.MeasureString(aboutMeText);
            Vector2 aboutMePosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (aboutMeBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) - (aboutMeBounds.Y * 2)
            );
            spriteBatch.DrawString(_font, aboutMeText, aboutMePosition, Color.White);

            #endregion

            #region AboutMe

            string classText = "Desenvolvimento de Jogos - Turma 2016";
            Vector2 classBounds = _font.MeasureString(classText);
            Vector2 classPosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (classBounds.Length() / 2),
                ((int) (graphics.Viewport.Height / 2) - (classBounds.Y * 2)) + aboutMeBounds.Y + 10
            );
            spriteBatch.DrawString(_font, classText, classPosition, Color.White);

            #endregion

            #region StartGameButton
            string backText = "Voltar";
            Vector2 backBounds = _fontTitle.MeasureString(backText);
            Vector2 backPosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (backBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) + (backBounds.Y * 2)
            );
            spriteBatch.DrawString(_fontTitle, backText, backPosition, Color.Blue);
            #endregion

            spriteBatch.End();
        }

        public void Update(GameTime gameTime)
        {
            GameManager.Players[PlayerIndex.One].Update();
            GameManager.Players[PlayerIndex.Two].Update();

            if (GameManager.Players[PlayerIndex.One].Enter() || GameManager.Players[PlayerIndex.Two].Enter()) {
                Game1.Scene = GameManager.ChangeState(GameState.MainMenu);
            }
        }

        public void LoadContent()
        {
        }

        public void Dispose()
        {
        }
    }
}