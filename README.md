# Trabalho de fisica
Desenvolvido em C# utilizando Monogame
## Como jogar

|       Comando      |    Player 1    |     Player 2      |
|--------------------|:--------------:|------------------:|
|     Acelerar       |        W       | Seta para Cima    |
|Girar para esquerda |        A       | Seta para Esquerda|
|Girar para direita  |        D       | Seta para Direita |
|     Tiro 1         |     Espaco     | Alt Direito       |
|     Tiro 2         |  CTRL Esquerdo | CTRL Direito      |

## Estrutura do Projeto
* GameFramework
    * GameObject
        * AbstractGameObject - Todos os objetos do jogo extendem essa classe, possui os metodos abstrados de draw e update
        * Weapon
            * Bullet - Tiro normal
            * HomingMissile - Tiro guiado
        * Ship - Classe das naves
    * Input
        * KeyboardInput - Wrapper para Input do Teclado
        * PlayerInput - Mapeamento das teclas
    * Physics - Classes de fisica
    * Scene
        *  IScene -  Todas as cenas implementam essa interface.
        *  Todas as cenas do jogo.
    *  Util
        *  MathUtil - Auxiliar para Matematica
* GameManager - Singleton para gerenciar as cenas, naves e jogadores.
* GameState - Enum para definir as cenas.