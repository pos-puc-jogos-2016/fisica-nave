﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UrquanClone.GameFramework.GameObject;

namespace UrquanClone.GameFramework.Scene
{
    public class ShipSelection : IScene
    {
        private readonly Background _background;

        private int _playerOneSelected = 0;
        private int _playerTwoSelected = 2;

        private bool _playerOneConfirmed = false;
        private bool _playerTwoConfirmed = false;
        private SpriteFont _font;

        public ShipSelection()
        {
            _background = new Background();
            _font = Game1.ContentManager.Load<SpriteFont>("Font/KenVectorFutureTitle");
        }

        public void LoadContent()
        {
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _background.Draw(gameTime, spriteBatch);

            spriteBatch.Begin();

            #region Title

            GraphicsDevice graphics = spriteBatch.GraphicsDevice;
            string titleText = "Selecione uma nave";
            Vector2 titleBounds = _font.MeasureString(titleText);
            Vector2 titlePosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (titleBounds.Length() / 2),
                40
            );
            spriteBatch.DrawString(_font, titleText, titlePosition, Color.White);

            #endregion

            spriteBatch.End();

            var i = 0;
            foreach (Ship ship in GameManager.Ships) {
                var p1 = false;
                var p2 = false;

                if (i == _playerOneSelected) {
                    p1 = true;
                }

                if (i == _playerTwoSelected) {
                    p2 = true;
                }

                ship.DrawPortrait(gameTime, spriteBatch, p1, p2);

                i++;
            }
        }

        public void Update(GameTime gameTime)
        {
            GameManager.Players[PlayerIndex.One].Update();
            GameManager.Players[PlayerIndex.Two].Update();

            if (!_playerOneConfirmed) {
                if (GameManager.Players[PlayerIndex.One].Left()) {
                    _playerOneSelected--;

                    if (_playerOneSelected < 0) {
                        _playerOneSelected = 2;
                    }
                }

                if (GameManager.Players[PlayerIndex.One].Right()) {
                    _playerOneSelected++;

                    if (_playerOneSelected > 2) {
                        _playerOneSelected = 0;
                    }
                }


                if (GameManager.Players[PlayerIndex.One].Enter()) {
                    _playerOneConfirmed = true;

                    GameManager.PlayerShips.Add(PlayerIndex.One, GameManager.Ships[_playerOneSelected]);
                }
            }


            if (!_playerTwoConfirmed) {
                if (GameManager.Players[PlayerIndex.Two].Left()) {
                    _playerTwoSelected--;

                    if (_playerTwoSelected < 0) {
                        _playerTwoSelected = 2;
                    }
                }

                if (GameManager.Players[PlayerIndex.Two].Right()) {
                    _playerTwoSelected++;

                    if (_playerTwoSelected > 2) {
                        _playerTwoSelected = 0;
                    }
                }


                if (GameManager.Players[PlayerIndex.Two].Enter()) {
                    _playerTwoConfirmed = true;

                    GameManager.Ships[_playerTwoSelected].Angle = MathHelper.ToRadians(180);
                    GameManager.PlayerShips.Add(PlayerIndex.Two, GameManager.Ships[_playerTwoSelected]);
                }
            }

            if (_playerOneConfirmed && _playerTwoConfirmed) {
                Game1.Scene = GameManager.ChangeState(GameState.Battle);
            }
        }

        public void Dispose()
        {
        }
    }
}