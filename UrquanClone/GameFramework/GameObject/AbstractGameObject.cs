﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UrquanClone.GameFramework.GameObject
{
    public abstract class AbstractGameObject : IDisposable
    {
        public Vector2 Position;
        protected Texture2D _sprite;
        public Vector2 MaxSpeed;
        public Vector2 Velocity;
        public Vector2 Direction;

        public abstract void LoadContent();
        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void Dispose();
    }
}