﻿using System;
using Microsoft.Xna.Framework;
using UrquanClone.GameFramework.GameObject;

namespace UrquanClone.GameFramework.Physics
{
    public class SteeringBehaviour
    {
        public static Vector2 Seek(AbstractGameObject gameObject, Vector2 target)
        {
            Vector2 desiredVelocity = target - gameObject.Position;
            desiredVelocity.Normalize();
            desiredVelocity *= gameObject.MaxSpeed;

            return desiredVelocity - gameObject.Velocity;
        }

        public static Vector2 Pursuit(AbstractGameObject pursuer, AbstractGameObject evader)
        {
            Vector2 toEvader = evader.Position - pursuer.Position;
            float relativeDir = Vector2.Dot(pursuer.Direction, evader.Direction);

            if (Vector2.Dot(toEvader, pursuer.Direction) > 0 && (relativeDir < Math.Acos(18))) {
                return Seek(pursuer, evader.Position);
            }


            ///??????
            //float lookAheadTime = toEvader / (pursuer.MaxSpeed + evader.Velocity);

            return Seek(pursuer, evader.Position);
        }
    }
}