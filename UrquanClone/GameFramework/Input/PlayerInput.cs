﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace UrquanClone.GameFramework.Input
{
    public class PlayerInput
    {
        public PlayerIndex Index { get; }
        private KeyboardInput _input = new KeyboardInput();

        public PlayerInput(PlayerIndex index)
        {
            this.Index = index;
        }

        public void Update()
        {
            _input.Update(Keyboard.GetState());
        }

        public bool Up()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.W);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.Up);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Down()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.S);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.Down);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Left()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.A);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.Left);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Right()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.D);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.Right);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Enter()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.Space);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.Enter);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool RotateLeft()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyHeld(Keys.A);
                case PlayerIndex.Two:
                    return _input.IsKeyHeld(Keys.Left);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool RotateRight()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyHeld(Keys.D);
                case PlayerIndex.Two:
                    return _input.IsKeyHeld(Keys.Right);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        public bool Thrust()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyHeld(Keys.W);
                case PlayerIndex.Two:
                    return _input.IsKeyHeld(Keys.Up);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Shot1()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.Space);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.RightAlt);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool Shot2()
        {
            switch (Index)
            {
                case PlayerIndex.One:
                    return _input.IsKeyPressed(Keys.LeftControl);
                case PlayerIndex.Two:
                    return _input.IsKeyPressed(Keys.RightControl);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}