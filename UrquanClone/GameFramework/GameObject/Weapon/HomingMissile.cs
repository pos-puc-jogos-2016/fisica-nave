﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UrquanClone.GameFramework.Physics;
using UrquanClone.GameFramework.Util;

namespace UrquanClone.GameFramework.GameObject.Weapon
{
    public class HomingMissile: Bullet
    {
        private readonly AbstractGameObject _thisShip;
        private readonly AbstractGameObject _target;

        public HomingMissile(AbstractGameObject thisShip, AbstractGameObject target)
        {
            _thisShip = thisShip;
            _target = target;
            _sprite = Game1.ContentManager.Load<Texture2D>("Weapon/laserRed07");
        }


        protected new Vector2 CalculateForces()
        {
            Vector2 forces = new Vector2();
            forces += MathUtil.NewBySizeAngle(0.05f, Angle);
            forces += SteeringBehaviour.Pursuit(_thisShip, _target);

            return forces;
        }
    }
}