﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UrquanClone.GameFramework.Scene
{
    public class GameOver : IScene
    {
        private readonly Background _background;
        private readonly SpriteFont _font;

        public GameOver()
        {
            _background = new Background();
            _font = Game1.ContentManager.Load<SpriteFont>("Font/KenVectorFutureTitle");
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _background.Draw(gameTime, spriteBatch);

            spriteBatch.Begin();
            GraphicsDevice graphics = spriteBatch.GraphicsDevice;


            #region Winner

            string winnerText = "Player " + GameManager.Winner + " Venceu";
            Vector2 winnerBounds = _font.MeasureString(winnerText);
            Vector2 winnerPosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (winnerBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) - (winnerBounds.Y * 2)
            );
            spriteBatch.DrawString(_font, winnerText, winnerPosition, Color.White);

            #endregion

            #region ReturnButton

            string backText = "Voltar";
            Vector2 backBounds = _font.MeasureString(backText);
            Vector2 backPosition = new Vector2(
                (int) (graphics.Viewport.Width / 2) - (backBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) + (backBounds.Y * 2)
            );
            spriteBatch.DrawString(_font, backText, backPosition, Color.Blue);

            #endregion

            spriteBatch.End();
        }

        public void Update(GameTime gameTime)
        {
            GameManager.Players[PlayerIndex.One].Update();
            GameManager.Players[PlayerIndex.Two].Update();

            if (GameManager.Players[PlayerIndex.One].Enter() || GameManager.Players[PlayerIndex.Two].Enter()) {
                GameManager.Reset();
                Game1.Scene = GameManager.ChangeState(GameState.MainMenu);
            }
        }

        public void LoadContent()
        {
        }

        public void Dispose()
        {
        }
    }
}