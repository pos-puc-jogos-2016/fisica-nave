﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using UrquanClone.GameFramework.GameObject;
using UrquanClone.GameFramework.Input;
using UrquanClone.GameFramework.Scene;

namespace UrquanClone
{
    public sealed class GameManager
    {
        private static readonly GameManager instance = new GameManager();

        public static Vector2 ScreenSize = new Vector2(800, 600);

        public static Dictionary<PlayerIndex, PlayerInput> Players = new Dictionary<PlayerIndex, PlayerInput>()
        {
            {PlayerIndex.One, new PlayerInput(PlayerIndex.One)},
            {PlayerIndex.Two, new PlayerInput(PlayerIndex.Two)}
        };

        public static Ship[] Ships =
        {
            new Ship(0), 
            new Ship(1), 
            new Ship(2)
        };

        public static Dictionary<PlayerIndex, Ship> PlayerShips = new Dictionary<PlayerIndex, Ship>();

        private GameManager()
        {
        }

        public static GameManager Instance => instance;
        public static int Winner { get; set; }

        public static GameState State = GameState.MainMenu;

        private static IScene _previouScene = null;

        private static IScene _currentScene = null;


        public static IScene ChangeState(GameState state)
        {
            _previouScene = _currentScene;
            switch (state) {
                case GameState.MainMenu:
                    _currentScene = new MainMenu();
                    break;
                case GameState.AboutMenu:
                    _currentScene = new About();
                    break;
                case GameState.ShipSelection:
                    _currentScene = new ShipSelection();
                    break;
                case GameState.Battle:
                    _currentScene = new Battle();
                    break;
                case GameState.GameOver:
                    _currentScene = new GameOver();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            Debug.WriteLine("Scene From " + _previouScene?.ToString() + "  - Scene To " + _currentScene?.ToString());

            _previouScene?.Dispose();

            return _currentScene;
        }


        public static void Reset()
        {
            PlayerShips.Remove(PlayerIndex.One);
            PlayerShips.Remove(PlayerIndex.Two);
            _previouScene.Dispose();
            _previouScene = null;
        }
    }
}