﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UrquanClone.GameFramework.GameObject.Weapon;
using UrquanClone.GameFramework.Input;
using UrquanClone.GameFramework.Util;

namespace UrquanClone.GameFramework.GameObject
{
    public class Ship : AbstractGameObject
    {
        private const float Mass = 1.0f;

        public int Life;

        public PlayerInput PlayerInput { get; set; }
        public Texture2D Sprite => _sprite;

        public int Index { get; }
        public float Angle { get; set; } = 0.0f;

        public Rectangle BoundingBox => new Rectangle((int)Position.X, (int)Position.Y, _sprite.Width, _sprite.Height);

        public Ship(int index)
        {
            Index = index;
            switch (index) {
                case 0:
                    _sprite = Game1.ContentManager.Load<Texture2D>("Ship/PlayerShipRed1");
                    Life = 10;
                    break;
                case 1:
                    _sprite = Game1.ContentManager.Load<Texture2D>("Ship/PlayerShipRed2");
                    Life = 50;
                    break;
                case 2:
                    _sprite = Game1.ContentManager.Load<Texture2D>("Ship/PlayerShipRed3");
                    Life = 25;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            MaxSpeed = new Vector2(500, 500);
        }

        public override void LoadContent()
        {
        }

        public override void Update(GameTime gameTime)
        {
            float secs = (float) gameTime.ElapsedGameTime.TotalSeconds;

            Velocity = Vector2.Clamp(Velocity, -MaxSpeed, MaxSpeed);

            Vector2 acellSecs = CalculateForces() / new Vector2(Mass * secs);
            Position += (Velocity + (acellSecs / 2.0f)) * secs;
            Velocity += acellSecs;

            Direction = Velocity;  
            Direction.Normalize();
            

            if (PlayerInput.RotateLeft()) {
                Angle -= MathUtil.ToRadians(180) * secs;
            }

            if (PlayerInput.RotateRight()) {
                Angle += MathUtil.ToRadians(180) * secs;
            }

            if (Position.X > GameManager.ScreenSize.X) {
                Position.X = 0;
            }

            if (Position.X < 0) {
                Position.X = GameManager.ScreenSize.X;
            }

            if (Position.Y > GameManager.ScreenSize.Y) {
                Position.Y = 0;
            }

            if (Position.Y < 0) {
                Position.Y = GameManager.ScreenSize.Y;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(_sprite, Position, null, Color.White, Angle,
                new Vector2((float) _sprite.Width / 2, (float) _sprite.Height / 2), 1.0f, SpriteEffects.None, 0.0f);
            spriteBatch.End();
        }

        public void DrawPortrait(GameTime gameTime, SpriteBatch spriteBatch, bool p1Selected, bool p2Selected)
        {
            spriteBatch.Begin();

            GraphicsDevice graphics = spriteBatch.GraphicsDevice;

            int xPos = (int) (graphics.Viewport.Width / 3);

            Vector2 position = new Vector2((int) (xPos / 3) + (xPos * Index), (int) (graphics.Viewport.Height / 2));


            Texture2D rect = new Texture2D(spriteBatch.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            rect.SetData<Color>(new Color[] {Color.White});

            if (p1Selected) {
                spriteBatch.Draw(rect, new Rectangle((int) position.X, (int) position.Y, Sprite.Width, Sprite.Height),
                    Color.DarkBlue);
            }

            if (p2Selected) {
                spriteBatch.Draw(rect, new Rectangle((int) position.X, (int) position.Y, Sprite.Width, Sprite.Height),
                    Color.DarkRed);
            }


            spriteBatch.Draw(Sprite, position, Color.White);
            spriteBatch.End();
        }

        private Vector2 CalculateForces()
        {
            Vector2 forces = new Vector2();

            if (PlayerInput.Thrust()) {
                forces += MathUtil.NewBySizeAngle(0.05f, Angle);
            }
            
            return forces;
        }

        public override void Dispose()
        {
        }
    }
}