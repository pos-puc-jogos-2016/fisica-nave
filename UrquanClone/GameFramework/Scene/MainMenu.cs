﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UrquanClone.GameFramework.Scene
{
    public class MainMenu : IScene
    {
        private readonly Background _background;
        private SpriteFont _font;
        private int _selectedOption = 0;


        public MainMenu()
        {
            _background = new Background();
            _font = Game1.ContentManager.Load<SpriteFont>("Font/KenVectorFutureTitle");
        }


        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            _background.Draw(gameTime, spriteBatch);

            spriteBatch.Begin();
            GraphicsDevice graphics = spriteBatch.GraphicsDevice;

            #region Title

            string titleText = "Urquan Clone";
            Vector2 titleBounds = _font.MeasureString(titleText);
            Vector2 titlePosition = new Vector2((int) (graphics.Viewport.Width / 2) - (titleBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) - (titleBounds.Y * 2));
            spriteBatch.DrawString(_font, titleText, titlePosition, Color.White);

            #endregion

            #region StartGameButton

            string startText = "Jogar";
            Vector2 startBounds = _font.MeasureString(titleText);
            Vector2 startPosition = new Vector2((int) (graphics.Viewport.Width / 2) - (startBounds.Length() / 2),
                (int) (graphics.Viewport.Height / 2) + (startBounds.Y * 2));
            spriteBatch.DrawString(_font, startText, startPosition, _selectedOption == 0 ? Color.Blue : Color.White);

            #endregion

            #region AboutButton

            string aboutText = "Sobre";
            Vector2 aboutBounds = _font.MeasureString(aboutText);
            Vector2 aboutPosition = new Vector2((int) (graphics.Viewport.Width / 2) + (aboutBounds.Length() / 4),
                (int) (graphics.Viewport.Height / 2) + (aboutBounds.Y * 2));
            spriteBatch.DrawString(_font, aboutText, aboutPosition, _selectedOption == 1 ? Color.Blue : Color.White);

            #endregion

            spriteBatch.End();
        }

        public void Update(GameTime gameTime)
        {
            _background.Update(gameTime);

            Debug.WriteLine("Update MainMenu");

            GameManager.Players[PlayerIndex.One].Update();
            GameManager.Players[PlayerIndex.Two].Update();

            if (GameManager.Players[PlayerIndex.One].Left() || GameManager.Players[PlayerIndex.Two].Left()) {
                _selectedOption -= 1;

                Debug.WriteLine("Up");

                if (_selectedOption < 0) {
                    _selectedOption = 1;
                }
            }

            if (GameManager.Players[PlayerIndex.One].Right() || GameManager.Players[PlayerIndex.Two].Right()) {
                _selectedOption += 1;

                Debug.WriteLine("Down");

                if (_selectedOption > 1) {
                    _selectedOption = 0;
                }
            }


            if (GameManager.Players[PlayerIndex.One].Enter() || GameManager.Players[PlayerIndex.Two].Enter()) {
                if (_selectedOption == 0) {
                    Game1.Scene = GameManager.ChangeState(GameState.ShipSelection);
                } else {
                    Game1.Scene = GameManager.ChangeState(GameState.AboutMenu);
                }
            }

            Debug.WriteLine(_selectedOption);
        }

        public void LoadContent()
        {
        }

        public void Dispose()
        {
        }
    }
}