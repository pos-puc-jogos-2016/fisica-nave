﻿namespace UrquanClone
{
    public enum GameState
    {
        MainMenu,
        AboutMenu,
        ShipSelection,
        Battle,
        GameOver
    }
}