﻿using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UrquanClone.GameFramework.Util;

namespace UrquanClone.GameFramework.GameObject.Weapon
{
    public class Bullet: AbstractGameObject
    {
        private Vector2 _momentum = new Vector2(0, 0);
        private const float Mass = 1.0f;
        private readonly Vector2 _maxSpeed = new Vector2(500, 500);

        public float Angle { get; set; } = 0.0f;

        public Rectangle BoundingBox => new Rectangle((int)Position.X, (int)Position.Y, _sprite.Width, _sprite.Height);

        public Bullet()
        {
            _sprite = Game1.ContentManager.Load<Texture2D>("Weapon/laserBlue07");
        }

        public override void LoadContent()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            float secs = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _momentum = Vector2.Clamp(_momentum, -_maxSpeed, _maxSpeed);

            Vector2 acellSecs = CalculateForces() / new Vector2(Mass * secs);
            Position += (_momentum + (acellSecs / 2.0f)) * secs;
            _momentum += acellSecs;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(_sprite, Position, null, Color.White, Angle,
                new Vector2((float)_sprite.Width / 2, (float)_sprite.Height / 2), 1.0f, SpriteEffects.None, 0.0f);
            spriteBatch.End();
        }

        public override void Dispose()
        {
        }

        protected Vector2 CalculateForces()
        {
            Vector2 forces = new Vector2();
            forces += MathUtil.NewBySizeAngle(0.05f, Angle);

            return forces;
        }
    }
}