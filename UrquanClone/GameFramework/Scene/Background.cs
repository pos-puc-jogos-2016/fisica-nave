﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace UrquanClone.GameFramework.Scene
{
    public class Background : IScene
    {
        private Texture2D _background;
        private Vector2 _position = Vector2.Zero;

        public Background()
        {
            this.LoadContent();
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(samplerState:SamplerState.LinearWrap);
            spriteBatch.Draw(_background, _position, new Rectangle(0, 0, spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height),Color.White);
            spriteBatch.End();
        }

        public void Update(GameTime gameTime)
        {
        }

        public void LoadContent()
        {
            _background = Game1.ContentManager.Load<Texture2D>("Background/black");
        }

        public void Dispose()
        {
            Game1.ContentManager.Dispose();
        }
    }
}