﻿using Microsoft.Xna.Framework.Input;

namespace UrquanClone.GameFramework.Input
{
    public class KeyboardInput
    {
        private KeyboardState _previousKeyState;
        public KeyboardState CurrentKeyState { get; private set; }

        public void Update(KeyboardState keyState)
        {
            _previousKeyState = CurrentKeyState;
            CurrentKeyState = keyState;
        }

        public bool IsKeyPressed(Keys key)
        {
            if (_previousKeyState.IsKeyUp(key) && CurrentKeyState.IsKeyDown(key))
                return true;

            return false;
        }

        public bool IsKeyHeld(Keys key)
        {
            if (_previousKeyState.IsKeyDown(key) && CurrentKeyState.IsKeyDown(key))
                return true;

            return false;
        }

        public bool IsKeyReleased(Keys key)
        {
            if (_previousKeyState.IsKeyDown(key) && CurrentKeyState.IsKeyUp(key))
                return true;

            return false;
        }
    }
}